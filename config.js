const config = {
    world: {
        style: {
            backgroundColor : '#f1f1f1',
            position : 'absolute'
        },
        
        misc : {
            lengthToTunnel: 0.2,//0.1 = 10% of the heigt or width.
            maxCavens: 20
        }
    },
    caven: {
        fill: 'rgba(0, 0, 0, 0.2)',
    },
    tunnel: {
        fill: 'rgba(0, 0, 0, 0.2)',
        lineWidth: 0.01,
    },
    goldLine : {
        fill: 	"#D4AF37",
        lineWidth: 0.01,
    },
    human : {
        fill: 'rgba(5, 5, 5, 0.2)',
    }
}
export default config