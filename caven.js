import config from './config.js'
export default class Caven {
    constructor() {
        this.x = Math.random()

        this.y = Math.random()
        if(this.y < 0.2) this.y = 0.2

        this.radius = 0.05
        this.neighbours = []
        this.creatures = []
        this.items = []
        if(Math.random()*100 > 25) {
            this.creatures.push({})
        }
        if(Math.random()*100 > 95) {
            this.items.push({name: 'Gem'})
        }
        this.reRender = true
    }
    preRender(canvas, ctx) {
        if(this.creatures.length > 0 && this.neighbours.length > 0 && (Math.random()* 100 > 50)) {
            let index = Math.floor(Math.random()*this.neighbours.length)
            index = index < 0 ? 0 : (index > this.neighbours.length - 1) ? this.neighbours.length - 1 : index
            const n = this.neighbours[index]
            n.creatures.push({})
            this.creatures.pop()
            n.reRender = true
            this.reRender = true
            this.clearCircle(canvas.width*this.x, canvas.height*this.y, (canvas.height*this.radius), ctx)
            n.clearCircle(n.x * canvas.width, n.y * canvas.height, (n.radius * canvas.height), ctx)
        }

    }

    clearCircle( x , y , r, ctx ){
        r += 1
        for( var i = 0 ; i < Math.round( Math.PI * r ) ; i++ ){
            var angle = ( i / Math.round( Math.PI * r )) * 360;
            ctx.clearRect( x , y , Math.sin( angle * ( Math.PI / 180 )) * r , Math.cos( angle * ( Math.PI / 180 )) * r );
        }
    }
    render(canvas, ctx) {
        if(this.reRender) {
            ctx.beginPath()
            ctx.fillStyle = config.caven.fill
            ctx.lineWidth = 0
            ctx.arc(canvas.width*this.x, canvas.height*this.y,canvas.height*this.radius, 0, 2*Math.PI)
            ctx.fill()
            if(this.creatures.length > 0) {
                const height = (canvas.height*this.radius)
                const width = (canvas.height*this.radius)
                const img = new Image()
                img.onload = () => {
                    ctx.drawImage(img, (canvas.width*this.x)-width/2, (canvas.height*this.y)-height/2, height, width )
                }
                img.src = "./img/monster.png"
            }
            this.reRender = false
            ctx.closePath()
        }

    }

    collide(caven) {
        let height = 20
        let width = 10

        let distX = (caven.x - this.x) * width
        let distY = (caven.y - this.y) * height
        let distance = Math.sqrt((distX * distX) + (distY * distY))
        return (distance <= ((caven.radius + this.radius)*height))
    }

    canTunnelBeteen(caven, value) {
        let height = window.innerHeight
        let width = window.innerWidth
        let distX = Math.abs((caven.x*width - this.x*width))
        let distY = Math.abs((caven.y*height - this.y*height))
        let min = Math.min((height * value), (width * value))
        return distX <= min && distY <= min
    }
}
