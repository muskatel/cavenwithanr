import config from './config.js'
export default class Tunnel {
    constructor(caven1, caven2) {
        caven1.neighbours.push(caven2)
        caven2.neighbours.push(caven1)
        this.caven1 = caven1
        this.caven2 = caven2

    }

    render(canvas, ctx) {
        const width = canvas.width
        const height = canvas.height
        ctx.beginPath()
        ctx.moveTo(this.caven1.x * width, this.caven1.y * height)
        ctx.lineTo(this.caven2.x * width, this.caven2.y * height)
        ctx.strokeStyle = config.tunnel.fill
        ctx.lineWidth = config.tunnel.lineWidth * width
        ctx.stroke()
        ctx.closePath()
        
    }
}
